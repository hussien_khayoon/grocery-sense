Django==1.7.3
djangorestframework==3.0.3
django-registration
django-registration-redux
django-bootstrap3
wsgiref==0.1.2
enum34