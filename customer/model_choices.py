import inspect
from enum import Enum

class ChoiceEnum(Enum):

    @classmethod
    def choices(cls):
        # get all members of the class
        members = inspect.getmembers(cls, lambda m: not(inspect.isroutine(m)))
        # filter down to just properties
        props = [m for m in members if not(m[0][:2] == '__')]
        # format into django choice tuple
        choices = tuple([(p[1].value, p[0]) for p in props])
        return choices

class FoodProfile(ChoiceEnum):
    __order__ = ''
    REGULAR      = 0
    VEGETARIAN   = 1
    VEGAN        = 2
    KOSHER       = 3
    HALAL        = 4
    CUSTOM       = 5
    

class HealthProfile(ChoiceEnum):
    __order__ = ''
    DONT_CARE      = 0
    CARES_SLIGHTLY = 1
    CARES_ALOT     = 2


class PriceProfile(ChoiceEnum):
    __order__ = ''
    FRUGAL  = 0
    MEDIUM  = 1
    PRICY   = 2