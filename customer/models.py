from django.db import models
from customer.model_choices import PriceProfile, FoodProfile, HealthProfile


class CustomerProfile(models.Model):
    isCompleted                = models.BooleanField(default=False)
    foodProfileValue           = models.IntegerField(null=True, choices=FoodProfile.choices())
    healthProfileValue         = models.IntegerField(null=True, choices=HealthProfile.choices())
    priceProfileValue          = models.IntegerField(null=True, choices=PriceProfile.choices())
    created_on                 = models.DateTimeField(auto_now=False, auto_now_add=True)
    updated_on                 = models.DateTimeField(auto_now=True, auto_now_add=True)