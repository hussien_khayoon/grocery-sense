from django.shortcuts import render_to_response, redirect
from django.template import RequestContext


def customerDashboard(request):
    # check if they are a first time user take them throgh the setup
    # for we'll keep track of everything in the session
  
    return render_to_response('customer/setup.html',
                              context_instance=RequestContext(request))  
