from rest_framework import serializers
from customer.models import CustomerProfile


class CustomerProfileSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = CustomerProfile
        fields = ( 'isCompleted', 'foodProfileValue', 'healthProfileValue', 'priceProfileValue' )
        
